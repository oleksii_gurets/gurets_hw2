<?php

class Worker
{
    private $name;
    private $age;
    private $salary;

    public function __construct($name, $age, $salary){

        $this->name = $name;
        if( $age >= 18 ){
            $this->age = $age;
        }else{
            echo 'Worker must be of legal age!';
            die();
        }
        $this->salary = $salary;

    }

    public function getName(){

        return $this->name;

    }

    public function getAge(){

        return $this->age;
        
    }

    public function getSalary(){

        return $this->salary;
        
    }
}